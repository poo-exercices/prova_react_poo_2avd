import { useState } from "react";
import  Dashboard  from "./components/Dashboard";
import { Header } from "./components/Header";
import { NewEventModel } from "./components/NewEventModal";
import GlobalStyle from './styles/global';

export function App() {

  const [isNewEventModalOpen, setIsNewEventModalOpen] =
  useState(false);

  function handleOpenNewEventModal() {
    setIsNewEventModalOpen(true);
  }

  function handleCloseNewEventModal() {
    setIsNewEventModalOpen(false);
  }

  return (
    <div className="App">
      <Header onOpenNewEventModal={handleOpenNewEventModal} />
      <Dashboard />

      <NewEventModel
        isOpen={isNewEventModalOpen}
        onRequestClose={handleCloseNewEventModal}
      />

    <GlobalStyle />
    </div>
  )
}




