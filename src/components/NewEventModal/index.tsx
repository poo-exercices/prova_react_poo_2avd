import { useState, FormEvent, useEffect } from "react";
import Modal from "react-modal";
import closeImg from "../../assets/close.svg";
import { Container } from "./styles";
import api from "../../services/api";

interface NewEventModelProps {
  isOpen: boolean;
  onRequestClose: () => void;
}

export function NewEventModel({
  isOpen,
  onRequestClose,
}: NewEventModelProps) {
  const [nomeevento, setNomeevento] = useState("");
  const [local, setLocal] = useState("");
  const [diasemana, setDiasemana] = useState("");
  const [horario, setHorario] = useState("");

  async function handleCreateNewEvent(event: FormEvent) {
    event.preventDefault();
    const data = {
      nomeevento,
      local,
      diasemana,
      horario
    };
    await api.post("/events", data);

    setNomeevento("");
    setLocal("");
    setDiasemana("");
    setHorario("");

    alert("Cadastro realizado com sucesso");
  }

  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={onRequestClose}
      overlayClassName="react-modal-overlay"
      className="react-modal-content"
    >
      <button
        type="button"
        className="react-modal-close"
        onClick={onRequestClose}
      >
        <img src={closeImg} alt="Fechar modal" />
      </button>
      <Container onSubmit={handleCreateNewEvent}>
        <h2>Novo Evento</h2>

        <input
          type='text' 
          placeholder='Nome Do Evento' 
          value={nomeevento}
          onChange={(event) => setNomeevento(event.target.value)}
        />

        <input
          placeholder="Local do Evento"
          type="text"
          value={local}
          onChange={(event) => setLocal(event.target.value)}
        />

        <input
          placeholder="Dia do Evento"
          type="text"
          value={diasemana}
          onChange={(event) => setDiasemana(event.target.value)}
        />

        <input
          placeholder="hora do Evento"
          type="text"
          value={horario}
          onChange={(event) => setHorario(event.target.value)}
        />

        <button type="submit">Cadastrar</button>
      </Container>
    </Modal>
  );
}