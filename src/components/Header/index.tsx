import logoImg from "../../assets/cal.png";
import { Container, Content } from "./styles";

// //criar props para receber a função que está no App
interface HeaderProps {
  onOpenNewEventModal: () => void;
}

export function Header({ onOpenNewEventModal }: HeaderProps) {
// export function Header() {
  return (
    <Container>
      <Content>
        <img src={logoImg} alt="prova_poo" />
        <button type="button" onClick={onOpenNewEventModal}>
          Novo Evento
        </button>
      </Content>
    </Container>
  );
}
